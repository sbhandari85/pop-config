import pathlib
import sys
import unittest.mock as mock

import pytest


@pytest.fixture(autouse=True, scope="module")
def add_to_path():
    """
    Add the code dir and pypath modules to the current python path
    """
    TPATH_DIR = str(pathlib.Path(__file__).parent.parent.absolute() / "pypath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        yield


@pytest.fixture(autouse=True, scope="module")
def clean_cli_args():
    with mock.patch("sys.argv", ["config"]):
        yield


@pytest.fixture(autouse=True, scope="module")
def clean_os_vars():
    with mock.patch("os.environ", {}):
        yield
