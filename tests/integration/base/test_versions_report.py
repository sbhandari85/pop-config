import yaml


def test_get_versions_report(cli):
    ret = cli("--versions-report")
    versions_report = yaml.safe_load(ret)
    assert versions_report["argv"].endswith("run.py")
    assert versions_report["config"]["pop_config"] == {
        "log_datefmt": "%H:%M:%S",
        "log_file": "run.log",
        "log_fmt_console": "[%(levelname)-8s] %(message)s",
        "log_fmt_logfile": "%(asctime)s,%(msecs)03d "
        "[%(name)-17s][%(levelname)-8s] %(message)s",
        "log_handler_options": [],
        "log_level": "warning",
        "log_plugin": "basic",
    }
    assert any("pop-config" in v for v in versions_report["freeze"])
    assert versions_report["name"] == "pop_config"
    assert versions_report["version"]
    assert versions_report["python"]
    assert versions_report["platform"]
    assert versions_report["locale"]
