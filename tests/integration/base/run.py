"""
A simple pop program to test pop-config base options
"""
import pop.hub

if __name__ == "__main__":
    hub = pop.hub.Hub()
    hub.pop.config.load(["pop_config", "rend"], cli="pop_config")
