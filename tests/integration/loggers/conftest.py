import os
import pathlib
import subprocess
import sys
import tempfile

import pytest


@pytest.fixture(name="logfile", scope="function")
def logfile_(tmpdir):
    fh = tempfile.NamedTemporaryFile(suffix=".log", delete=False, dir=tmpdir)
    path = pathlib.Path(fh.name)
    yield path


def run_cli(*args, env=None):
    if env is None:
        env = {}

    if os.name == "nt" and "SYSTEMROOT" not in env:
        env["SYSTEMROOT"] = os.getenv("SYSTEMROOT")

    runpy = pathlib.Path(__file__).parent / "run.py"
    command = [sys.executable, str(runpy), *args]

    proc = subprocess.Popen(command, encoding="utf-8", env=env, stderr=subprocess.PIPE)
    assert proc.wait() == 0, proc.stderr

    # We only care about what was printed to the logs
    return proc.stderr


@pytest.fixture(name="cli", scope="function")
def cli():
    return run_cli
