import pytest

try:
    import pywin32  # noqa

    HAS_LIBS = True, ""
except ImportError as e:
    HAS_LIBS = False, str(e)


@pytest.mark.skipif(not HAS_LIBS[0], reason=HAS_LIBS[1])
@pytest.mark.skip("This test must be run as an administrator")
def test_nt_event(cli):
    stderr = cli(
        "--log-plugin=nt_event",
        "--log-level=trace",
    )

    # For now it's good enough that it printed to stderr and didn't raise an error
    assert "[TRACE   ] trace\n" in stderr
